const buttonStop = document.querySelector('#stop-button');
const buttonStart = document.querySelector('#start-button');

const pictures = document.querySelectorAll('.image-to-show');

let currentIndex = 0;
let timer = null;


const startPicturePreview = (array, duration = 1000,) =>{
    buttonStop.classList.add('show');
    buttonStart.classList.remove('show');

    
    timer = setInterval(() => {
        array.forEach(el => el.classList.remove('show'));
        const item = array[currentIndex];
        item.classList.add('show');
        currentIndex++;
        if(currentIndex === pictures.length) {
            currentIndex = 0;
        }
    }, duration);
}

const stopPicturePreview = () => {
    clearInterval(timer);
    console.log(timer)
    buttonStart.classList.add('show');
    buttonStop.classList.remove('show');
}

buttonStop.addEventListener('click', stopPicturePreview);
buttonStart.addEventListener('click', () => startPicturePreview(pictures));

startPicturePreview(pictures);




